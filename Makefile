# Variables that can safely be changed
CONTENT_DIR=content
BUILD_DIR=_build

# These are only to make the recipes clearer by not using long inline variables
DUCKS=$(wildcard $(CONTENT_DIR)/*.duck)
PAGES=$(DUCKS:$(CONTENT_DIR)/%.duck=$(BUILD_DIR)/%.page)

.PHONY: content yelp html clean

content: $(BUILD_DIR) $(PAGES)

$(BUILD_DIR)/%.page: $(CONTENT_DIR)/%.duck
	@ducktype $< -o $(BUILD_DIR)

$(BUILD_DIR):
	@mkdir -p $@

yelp: content
	@echo "En attente de la fermeture de yelp…"
	@yelp --editor-mode $(BUILD_DIR) 1>/dev/null 2>&1

html: content $(BUILD_DIR)/html
	@yelp-build html -o $(BUILD_DIR)/html $(BUILD_DIR)/*.page

$(BUILD_DIR)/html:
	@mkdir -p $@

clean: $(BUILD_DIR)
	@rm -rf $(BUILD_DIR)
