# Guide de l’équipe de traduction française

Cette documentation recueille les informations sur les procédures et bonnes pratiques en vigueur dans l’équipe de traduction française de GNOME.
Elle vise à remplacer le [wiki actuel](https://www.traduc.org/gnomefr/Presentation) mais n’est pour l’instant pas aussi complète.

## Utilisation de ce guide

Après avoir installé ducktype avec `pip install --user mallard-ducktype`, vous pouvez construire le guide en lançant la commande `make` depuis le dossier racine.
Pour ouvrir le guide, lancez la commande `make yelp`.

## Licence

Ce guide est publié sous licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
Vous pouvez le modifier et le partager tant que vous citez les auteurs et la licence, et en conservant cette licence pour toute modification faite.
